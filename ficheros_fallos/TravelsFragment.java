package com.drida.asrent.presentation.travels;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.util.Log;
import android.view.View;

import com.drida.asrent.IntentStarter;
import com.drida.asrent.Pruebas;
import com.drida.asrent.R;
import com.drida.asrent.ShareApplication;
import com.drida.asrent.base.view.BaseTravelsFragment;
import com.drida.asrent.dagger.NavigationModule;

import javax.inject.Inject;

public class TravelsFragment extends BaseTravelsFragment<TravelsView, TravelsPresenter>
        implements TravelsView, TravelsAdapter.TravelClickedListener {


    @Inject
    IntentStarter intentStarter;

    TravelsComponent travelsComponent;


    @Override
    protected int getLayoutRes() {
        return R.layout.fragment_travels_base;
    }

    @Override
    public void onViewCreated(View view, @Nullable Bundle savedIntanceState) {
        super.onViewCreated(view, savedIntanceState);
    }

    @Override
    public TravelsPresenter createPresenter() {
        injectDependencies();
        return travelsComponent.presenter();

    }




    @Override
    public void loadData(boolean pullToRefresh) {
        Log.d("ENTRO", "Entro loadData() BaseTravelsFragment");
        presenter.load(pullToRefresh, Pruebas.PUBLICADOR);
    }

    @Override
    protected void injectDependencies() {
        travelsComponent = DaggerTravelsComponent.builder()
                .shareAppComponent(ShareApplication.getShareComponents())
                .navigationModule(new NavigationModule())
                .build();
        travelsComponent.inject(this);
    }

    @Override
    public void showLoading(boolean pullToRefresh) {
        super.showLoading(pullToRefresh);

    }

    @Override
    public void showError(Throwable e, boolean pullToRefresh) {
        super.showError(e, pullToRefresh);
        Log.d("ENTRO", "Entro showError() BaseTravelsFragment");

    }

    @Override
    public void showContent() {
        super.showContent();
        Log.d("ENTRO", "Entro showContent() BaseTravelsFragment");

    }

    @Override
    public void onStart() {
        super.onStart();

    }

    @Override
    public void onStop() {

        super.onStop();

    }
}