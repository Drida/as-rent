package com.drida.asrent.data.repository;

import com.drida.asrent.data.specification.Specification;
import com.drida.asrent.domain.model.contact.Person;

import java.util.List;

import io.reactivex.Observable;

public interface IPersonRepository {

    Observable<Person> addPerson(Person person);

    Observable<List<Person>> addPerson(List<Person> persons);

    Observable<Person> updatePerson(Person person);

    Observable<Person> removePerson(Person person);

    Observable<List<Person>> removePerson(Specification specification);

    Observable<List<Person>> queryPerson(Specification specification);

}
