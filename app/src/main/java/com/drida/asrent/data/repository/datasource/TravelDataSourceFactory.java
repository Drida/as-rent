package com.drida.asrent.data.repository.datasource;

import com.drida.asrent.data.cloud.CloudImpl;
import com.drida.asrent.data.repository.datasource.core.DataSource;
import com.google.firebase.database.FirebaseDatabase;

import javax.inject.Inject;

public class TravelDataSourceFactory {

    FirebaseDatabase database;

    @Inject
    public TravelDataSourceFactory(FirebaseDatabase database) {
        this.database = database;
    }

    public DataSource createDataSource(){

        CloudImpl cloud = new CloudImpl(database);

        return new TravelCloudApiDataSource(cloud);
    }

}
