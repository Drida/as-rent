package com.drida.asrent.data.repository.datasource;

import com.drida.asrent.data.cloud.CloudApi;
import com.drida.asrent.data.entity.AccountEntity;
import com.drida.asrent.data.entity.PersonEntity;
import com.drida.asrent.data.repository.datasource.core.DataSource;
import com.google.firebase.auth.AuthCredential;

import io.reactivex.Observable;


public class AccountCloudApiIDataSource extends DataSource {

    private final CloudApi cloudApi;

    public AccountCloudApiIDataSource(CloudApi cloudApi) {
        this.cloudApi = cloudApi;
    }


    @Override
    public Observable<AccountEntity> logInUser(String email, String password) {
        return this.cloudApi.logInUser(email, password);
    }

    @Override
    public Observable<AccountEntity> logInUserFacebook(AuthCredential credential) {
        return this.cloudApi.logInUserFacebook(credential);
    }

    @Override
    public Observable<AccountEntity> logOutUser() {
        return this.cloudApi.logOutUser();
    }

    @Override
    public AccountEntity getUserCurrent() {
        return this.cloudApi.getCurrentUser();
    }

    @Override
    public boolean isUserAuhtenticated() {
        return this.cloudApi.isUserAuthenticated();
    }

    @Override
    public Observable<AccountEntity> registredUser(PersonEntity item) {
        return this.cloudApi.registrerUser(item);
    }


}
