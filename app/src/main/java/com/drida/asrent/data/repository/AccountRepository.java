package com.drida.asrent.data.repository;

import com.drida.asrent.data.entity.PersonEntity;
import com.drida.asrent.data.repository.datasource.AccountDataSourceFactory;
import com.drida.asrent.data.repository.datasource.core.IDataSource;
import com.drida.asrent.data.repository.datasource.mapper.AccountToAccountEntityMapper;
import com.drida.asrent.data.repository.datasource.mapper.PersonToPersonEntityMapper;
import com.drida.asrent.domain.model.account.Account;
import com.drida.asrent.domain.model.contact.Person;


import com.google.firebase.auth.AuthCredential;

import javax.inject.Inject;

import io.reactivex.Observable;


public class AccountRepository implements IAccountIRepository {


    private final PersonToPersonEntityMapper personToPersonEntityMapper;
    private final AccountToAccountEntityMapper accountToAccountEntityMapper;
    private final IDataSource dataSource;


    @Inject
    AccountRepository(AccountDataSourceFactory accountDataSourceFactory,
                      AccountToAccountEntityMapper accountToAccountEntityMapper,
                      PersonToPersonEntityMapper personToPersonEntityMapper) {

        this.dataSource = accountDataSourceFactory.createDataSource();
        this.accountToAccountEntityMapper = accountToAccountEntityMapper;
        this.personToPersonEntityMapper = personToPersonEntityMapper;


    }


    @Override
    public Observable<Account> logInUser(String email, String password) {

        return dataSource.logInUser(email, password).map(accountToAccountEntityMapper::reverseMap);

    }

    public Observable<Account> logInUserFacebook(AuthCredential credential) {

        return dataSource.logInUserFacebook(credential).map(accountToAccountEntityMapper::reverseMap);

    }

    @Override
    public Observable<Account> logOutUser() {
        return dataSource.logOutUser().map(accountToAccountEntityMapper::reverseMap);
    }

    @Override
    public Account getUserCurrent() {

        return accountToAccountEntityMapper.reverseMap(dataSource.getUserCurrent());
    }

    @Override
    public boolean isUserAuhtenticated() {
        return dataSource.isUserAuhtenticated();
    }

    @Override
    public Observable<Account> registredUser(Person item) {

        PersonEntity itemAux = personToPersonEntityMapper.map(item);
        return dataSource.registredUser(itemAux).map(accountToAccountEntityMapper::reverseMap);
    }


}
