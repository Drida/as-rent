package com.drida.asrent.data.repository.datasource;

import com.drida.asrent.data.cloud.CloudApi;
import com.drida.asrent.data.entity.AccountEntity;
import com.drida.asrent.data.entity.TravelEntity;
import com.drida.asrent.data.repository.datasource.core.DataSource;
import com.drida.asrent.data.specification.Specification;
import com.drida.asrent.domain.model.travel.RequestTravel;
import com.drida.asrent.domain.model.travel.SearchTravel;
import com.drida.asrent.domain.model.travel.Travel;

import java.util.List;

import io.reactivex.Observable;
import io.reactivex.Single;

public class TravelCloudApiDataSource extends DataSource {

    private final CloudApi cloudApi;

    public TravelCloudApiDataSource(CloudApi cloudApi) {
        this.cloudApi = cloudApi;
    }


    @Override
    public Observable<TravelEntity> addTravel(TravelEntity travel) {
        return this.cloudApi.addTravel(travel);
    }

    @Override
    public Observable<List<TravelEntity>> addTravel(List<TravelEntity> travels) {
        return this.cloudApi.addTravel(travels);
    }

    @Override
    public Observable<TravelEntity> updateTravel(TravelEntity travel) {
        return this.cloudApi.updateTravel(travel);
    }

    @Override
    public Observable<TravelEntity> removeTravel(TravelEntity travel) {
        return this.cloudApi.removeTravel(travel);
    }

    @Override
    public Observable<List<TravelEntity>> removeTravel(Specification specification) {
        return this.cloudApi.removeTravel(specification);
    }

    @Override
    public  Observable<List<TravelEntity>> queryTravel(Specification specification) {
        return this.cloudApi.queryTravel(specification);
    }

    @Override
    public Observable<List<TravelEntity>> queryTravelBySearch(SearchTravel searchTravel) {
        return this.cloudApi.queryTravelBySearch(searchTravel);
    }

    @Override
    public Observable<TravelEntity> requestTravel(RequestTravel requesTravel) {
        return this.cloudApi.requestTravel(requesTravel);
    }

    @Override
    public Observable<List<TravelEntity>> queryTravelsByApplicant(String uidApplicant) {
        return this.cloudApi.queryTravelsByApplicant(uidApplicant);
    }

    @Override
    public Observable<List<TravelEntity>> queryTravelsByPublisher(String uidPublisher) {
       return this.cloudApi.queryTravelsByPublisher(uidPublisher);
    }
}
