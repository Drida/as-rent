package com.drida.asrent.data.specification.firebase;

import com.drida.asrent.data.cloud.FirebaseReferences;
import com.drida.asrent.data.entity.PersonEntity;
import com.drida.asrent.domain.model.contact.Person;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.Query;
import com.google.firebase.database.ValueEventListener;

import java.util.ArrayList;
import java.util.List;

import io.reactivex.ObservableEmitter;

public class PersonByIdSpecification implements FirebaseSpecification{

    String idPerson;

    public PersonByIdSpecification(String idPerson) {
        this.idPerson = idPerson;
    }



    @Override
    public Query toFirebaseQuery() {
        Query query = FirebaseDatabase.getInstance().getReference()
                .child(FirebaseReferences.USERS_REFERENCE)
                .orderByChild(FirebaseReferences.USER_UID_REFERENCE)
                .equalTo(idPerson);


        return query;
    }
}
