package com.drida.asrent.data.repository;


import com.drida.asrent.domain.model.account.Account;
import com.drida.asrent.domain.model.contact.Person;

import com.google.firebase.auth.AuthCredential;

import io.reactivex.Observable;

public interface IAccountIRepository  {

    Observable<Account> logInUser(String email, String password);

    Observable<Account> logInUserFacebook(AuthCredential credential);

    Observable<Account> logOutUser();

    boolean isUserAuhtenticated();

    Account getUserCurrent();

    Observable<Account> registredUser(Person item);

}
