package com.drida.asrent.data.repository.datasource;

import com.drida.asrent.data.cloud.CloudImpl;
import com.drida.asrent.data.repository.datasource.core.IDataSource;
import com.google.firebase.database.FirebaseDatabase;

import javax.inject.Inject;

public class PersonDataSourceFactory {

    FirebaseDatabase database;

    @Inject
    public PersonDataSourceFactory(FirebaseDatabase database) {
        this.database = database;

    }

    public IDataSource createDataSource() {


        CloudImpl cloud = new CloudImpl(database);

        return new PersonCloudApiIDataSource(cloud);

    }

}
