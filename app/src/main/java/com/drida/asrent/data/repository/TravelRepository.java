package com.drida.asrent.data.repository;

import com.drida.asrent.data.entity.TravelEntity;
import com.drida.asrent.data.repository.datasource.TravelDataSourceFactory;
import com.drida.asrent.data.repository.datasource.core.IDataSource;
import com.drida.asrent.data.repository.datasource.mapper.TravelToTravelEntityMapper;
import com.drida.asrent.data.specification.Specification;

import com.drida.asrent.domain.model.travel.RequestTravel;
import com.drida.asrent.domain.model.travel.SearchTravel;
import com.drida.asrent.domain.model.travel.Travel;

import java.util.List;

import javax.inject.Inject;

import io.reactivex.Observable;

public class TravelRepository implements ITravelRepository {

    private final TravelToTravelEntityMapper travelToTravelEntityMapper;
    private final IDataSource dataSource;

    @Inject
    public TravelRepository(TravelDataSourceFactory travelDataSourceFactory,
                            TravelToTravelEntityMapper travelToTravelEntityMapper) {

        this.travelToTravelEntityMapper = travelToTravelEntityMapper;
        this.dataSource = travelDataSourceFactory.createDataSource();

    }


    @Override
    public Observable<Travel> addTravel(Travel travel) {

        TravelEntity travelAux = travelToTravelEntityMapper.map(travel);
        return dataSource.addTravel(travelAux).map(travelToTravelEntityMapper::reverseMap);
    }

    @Override
    public Observable<List<Travel>> addTravel(List<Travel> travels) {

        List<TravelEntity> travelsAux = travelToTravelEntityMapper.map(travels);
        return dataSource.addTravel(travelsAux).map(travelToTravelEntityMapper::reverseMap);
    }

    @Override
    public Observable<Travel> updateTravel(Travel travel) {
        TravelEntity travelAux = travelToTravelEntityMapper.map(travel);
        return dataSource.updateTravel(travelAux).map(travelToTravelEntityMapper::reverseMap);
    }

    @Override
    public Observable<Travel> removeTravel(Travel travel) {

        TravelEntity travelAux = travelToTravelEntityMapper.map(travel);
        return dataSource.removeTravel(travelAux).map(travelToTravelEntityMapper::reverseMap);
    }

    @Override
    public Observable<List<Travel>> removeTravel(Specification specification) {

        return dataSource.removeTravel(specification).map(travelToTravelEntityMapper::reverseMap);

    }

    @Override
    public  Observable<List<Travel>> queryTravel(Specification specification) {

        return dataSource.queryTravel(specification).map(travelToTravelEntityMapper::reverseMap);
    }

    @Override
    public Observable<List<Travel>> queryTravelBySearch(SearchTravel searchTravel) {
        return dataSource.queryTravelBySearch(searchTravel).map(travelToTravelEntityMapper::reverseMap);
    }

    @Override
    public Observable<Travel> requestTravel(RequestTravel requestTravel) {
        return dataSource.requestTravel(requestTravel).map(travelToTravelEntityMapper::reverseMap);
    }

    @Override
    public Observable<List<Travel>> queryTravelsByApplicant(String uidApplicant) {
        return dataSource.queryTravelsByApplicant(uidApplicant).map(travelToTravelEntityMapper::reverseMap);
    }

    @Override
    public Observable<List<Travel>> queryTravelsByPublisher(String uidPublisher) {
        return dataSource.queryTravelsByPublisher(uidPublisher).map(travelToTravelEntityMapper::reverseMap);
    }
}
