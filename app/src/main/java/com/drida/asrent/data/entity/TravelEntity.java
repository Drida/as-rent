package com.drida.asrent.data.entity;

import java.util.HashMap;
import java.util.Map;

public class TravelEntity {

    private String uid;
    private String date_departure;
    private String date_arrival;
    private double available_space;
    private double price_weight;
    private String location_origin;
    private String location_destination;
    private String hour_departure;
    private String hour_arrival;
    private String description;
    private String uid_publisher;
    private String uid_applicant;
    private String type_weight;
    private double space_requested;
    private Map<String, Boolean> users_applicant = new HashMap<String, Boolean>();


    private PersonEntity publisher;
    private PersonEntity applicant;

    public TravelEntity() {
    }

    public TravelEntity(String uid, String date_departure, double available_space, double price_weight, String location_origin, String location_destination, String hour_departure, String hour_arrival, String description, String uid_publisher, String uid_applicant, String type_weight, PersonEntity publisher) {
        this.uid = uid;
        this.date_departure = date_departure;
        this.available_space = available_space;
        this.price_weight = price_weight;
        this.location_origin = location_origin;
        this.location_destination = location_destination;
        this.hour_departure = hour_departure;
        this.hour_arrival = hour_arrival;
        this.description = description;
        this.uid_publisher = uid_publisher;
        this.uid_applicant = uid_applicant;
        this.type_weight = type_weight;
        this.publisher = publisher;
    }

    public String getUid() {
        return uid;
    }

    public void setUid(String uid) {
        this.uid = uid;
    }

    public String getDate_departure() {
        return date_departure;
    }

    public void setDate_departure(String date_departure) {
        this.date_departure = date_departure;
    }

    public double getAvailable_space() {
        return available_space;
    }

    public void setAvailable_space(double available_space) {

        this.available_space = available_space;
    }

    public double getSpace_requested() {
        return space_requested;
    }

    public void setSpace_requested(double space_requested) {
        this.space_requested = space_requested;
    }

    public String getDate_arrival() {
        return date_arrival;
    }

    public void setDate_arrival(String date_arrival) {
        this.date_arrival = date_arrival;
    }

    public PersonEntity getApplicant() {
        return applicant;
    }

    public void setApplicant(PersonEntity applicant) {
        this.applicant = applicant;
    }

    public double getPrice_weight() {
        return price_weight;
    }

    public String getUid_applicant() {
        return uid_applicant;
    }

    public void setUid_applicant(String uid_applicant) {
        this.uid_applicant = uid_applicant;
    }

    public void setPrice_weight(double price_weight) {
        this.price_weight = price_weight;
    }

    public String getLocation_origin() {
        return location_origin;
    }

    public void setLocation_origin(String location_origin) {
        this.location_origin = location_origin;
    }

    public String getLocation_destination() {
        return location_destination;
    }

    public void setLocation_destination(String location_destination) {
        this.location_destination = location_destination;
    }

    public String getHour_departure() {
        return hour_departure;
    }

    public void setHour_departure(String hour_departure) {
        this.hour_departure = hour_departure;
    }

    public String getHour_arrival() {
        return hour_arrival;
    }

    public void setHour_arrival(String hour_arrival) {
        this.hour_arrival = hour_arrival;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public String getUid_publisher() {
        return uid_publisher;
    }

    public void setUid_publisher(String uid_publisher) {
        this.uid_publisher = uid_publisher;
    }

    public PersonEntity getPublisher() {
        return publisher;
    }

    public void setPublisher(PersonEntity publisher) {
        this.publisher = publisher;
    }

    public String getType_weight() {
        return type_weight;
    }

    public void setType_weight(String type_weight) {
        this.type_weight = type_weight;
    }

    public Map<String, Boolean> getUsers_applicant() {
        return users_applicant;
    }

    public void setUsers_applicant(Map<String, Boolean> users_applicant) {
        this.users_applicant = users_applicant;
    }
}

