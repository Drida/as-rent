package com.drida.asrent.data.cloud;

import android.support.annotation.NonNull;
import android.util.Log;

import com.drida.asrent.data.entity.TravelEntity;
import com.drida.asrent.data.specification.firebase.FirebaseSpecification;


import com.drida.asrent.data.specification.Specification;
import com.drida.asrent.data.entity.AccountEntity;
import com.drida.asrent.data.entity.PersonEntity;

import com.drida.asrent.data.specification.firebase.TravelByIdSpecification;
import com.drida.asrent.domain.model.travel.RequestTravel;
import com.drida.asrent.domain.model.travel.SearchTravel;
import com.drida.asrent.domain.model.travel.Travel;
import com.drida.asrent.utils.Dates;
import com.drida.asrent.utils.Hours;
import com.google.android.gms.tasks.OnFailureListener;
import com.google.firebase.auth.AuthCredential;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseUser;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.Query;
import com.google.firebase.database.ValueEventListener;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.concurrent.Callable;
import java.util.function.BiConsumer;

import io.reactivex.Observable;
import io.reactivex.ObservableEmitter;
import io.reactivex.ObservableOnSubscribe;
import io.reactivex.ObservableSource;
import io.reactivex.Observer;
import io.reactivex.functions.Function;


public class CloudImpl implements CloudApi {


    private FirebaseAuth authentication;
    private FirebaseDatabase database;

    public CloudImpl(FirebaseAuth authentication, FirebaseDatabase database) {
        this.authentication = authentication;
        this.database = database;
    }

    public CloudImpl(FirebaseAuth authentication) {
        this.authentication = authentication;
    }

    public CloudImpl(FirebaseDatabase database) {
        this.database = database;
    }


    @Override
    public Observable<AccountEntity> logInUser(String email, String password) {

        return Observable.create(emtter -> {
            authentication.signInWithEmailAndPassword(email, password)
                    .addOnCompleteListener(task -> {
                                if (task.isSuccessful()) {

                                    AccountEntity accountCurrent = new AccountEntity(
                                            authentication.getCurrentUser().getUid(),
                                            email,
                                            password);


                                    emtter.onNext(accountCurrent);
                                    emtter.onComplete();

                                } else {
                                    emtter.onError(new Throwable("Error logInUser CloudImpl"));
                                }

                            }

                    );

        });

    }

    @Override
    public Observable<AccountEntity> logInUserFacebook(AuthCredential credential) {

        return Observable.create(emtter -> {
            authentication.signInWithCredential(credential)
                    .addOnCompleteListener(task -> {

                                if (task.isSuccessful()) {

                                    AccountEntity accountCurrent = new AccountEntity(
                                            authentication.getCurrentUser().getUid(),
                                            authentication.getCurrentUser().getEmail(),
                                            "");


                                    emtter.onNext(accountCurrent);
                                    emtter.onComplete();

                                } else {
                                    emtter.onError(new Throwable("Error logInUser CloudImpl"));
                                }

                            }

                    );

        });

    }

    @Override
    public Observable<AccountEntity> registrerUser(PersonEntity item) {

        return Observable.create(emtter -> {


            authentication.createUserWithEmailAndPassword(item.getEmail(), item.getPassword())
                    .addOnCompleteListener(task -> {
                                if (task.isSuccessful()) {

                                    AccountEntity accountCurrent = new AccountEntity(
                                            authentication.getCurrentUser().getUid(),
                                            item.getEmail(),
                                            item.getPassword());


                                    item.setUid(authentication.getCurrentUser().getUid());

                                    DatabaseReference databaseReference = database.getReference(FirebaseReferences.USERS_REFERENCE);


                                    Map<String, Object> map = new HashMap<>();
                                    map.put(item.getUid(), item);

                                    databaseReference.updateChildren(map);

                                    emtter.onNext(accountCurrent);
                                    emtter.onComplete();

                                } else {
                                    emtter.onError(new Throwable("Error registro Usuario"));
                                }

                            }
                    );

        });
    }

    @Override
    public AccountEntity getCurrentUser() {


        FirebaseUser user = authentication.getCurrentUser();

        if (user != null) {

            AccountEntity currentUser = new AccountEntity(user.getUid(), user.getEmail(), "");
            return currentUser;
        } else {
            return new AccountEntity();
        }

    }

    @Override
    public boolean isUserAuthenticated() {
        FirebaseUser user = authentication.getCurrentUser();

        if (user != null) {
            return true;
        } else {
            return false;
        }
    }

    @Override
    public Observable<AccountEntity> logOutUser() {
        return Observable.create(emitter -> {

            FirebaseUser user = authentication.getCurrentUser();

            if (user != null) {
                authentication.signOut();

                AccountEntity currentUser = new AccountEntity(user.getUid(), user.getEmail(), "");

                Observable.just(currentUser);
            } else {
                Observable.error(new Throwable(""));
            }

        });
    }


    @Override
    public Observable<PersonEntity> addPerson(PersonEntity item) {


        return Observable.create(emtter -> {


            authentication.createUserWithEmailAndPassword(item.getEmail(), item.getPassword())
                    .addOnCompleteListener(task -> {
                                if (task.isSuccessful()) {

                                    String uidCurrentUser = authentication.getCurrentUser().getUid();

                                    item.setUid(uidCurrentUser);

                                    DatabaseReference reference = database.getReference(FirebaseReferences.USERS_REFERENCE);


                                    reference.setValue(uidCurrentUser);

                                    reference.child(uidCurrentUser).setValue(item);


                                    Observable.just(item);

                                } else {
                                    Observable.error(new Throwable(""));
                                }

                            }
                    );

        });

    }

    @Override
    public Observable<List<PersonEntity>> addPerson(List<PersonEntity> items) {


        return Observable.create(emitter -> {

            List<PersonEntity> listPersons = new ArrayList<>();

            for (PersonEntity person : items) {
                addPerson(person);
                listPersons.add(person);
            }

            emitter.onNext(listPersons);
            emitter.onComplete();


        });


    }

    @Override
    public Observable<PersonEntity> updatePerson(PersonEntity item) {

        return Observable.create(emitter -> {


            DatabaseReference reference = database.getReference(FirebaseReferences.USERS_REFERENCE);


            Map<String, Object> map = new HashMap<>();
            map.put(item.getUid(), item);
            reference.updateChildren(map, (databaseError, databaseReference) -> {

                Log.d("ENTRO", "ENtro updatePerson onComplete");
                if (databaseError == null) {
                    Log.d("ENTRO", "ENtro updatePerson ok");
                    emitter.onNext(item);
                    emitter.onComplete();
                } else {
                    Log.d("ENTRO", "ENtro updatePerson error");
                    emitter.onError(new Throwable(databaseError.getMessage()));
                }

            });


        });

    }

    @Override
    public Observable<PersonEntity> removePerson(PersonEntity item) {

        return Observable.create(emitter -> {


            DatabaseReference reference = database.getReference(FirebaseReferences.USERS_REFERENCE).child(item.getUid());

            reference.removeValue(new DatabaseReference.CompletionListener() {
                @Override
                public void onComplete(DatabaseError databaseError, DatabaseReference databaseReference) {
                    if (databaseError != null) {
                        emitter.onNext(item);
                        emitter.onComplete();
                    } else {
                        emitter.onError(new Throwable(databaseError.getMessage()));
                    }
                }
            });
        });

    }

    @Override
    public Observable<List<PersonEntity>> removePerson(Specification specification) {


        throw new UnsupportedOperationException();
    }

    @Override
    public Observable<List<PersonEntity>> queryPerson(Specification specification) {

        final FirebaseSpecification firebaseSpecification = (FirebaseSpecification) specification;

        Query query = firebaseSpecification.toFirebaseQuery();

        return Observable.create(emitter -> {

            query.addListenerForSingleValueEvent(new ValueEventListener() {
                @Override
                public void onDataChange(DataSnapshot dataSnapshot) {
                    Log.d("ENTRO", "Entro queryPerson onDataChange");

                    List<PersonEntity> personsList = new ArrayList<PersonEntity>();

                    for (DataSnapshot postSnapshot : dataSnapshot.getChildren()) {
                        Log.d("ENTRO", "Entro BUCLE FOR queryPerson onData Change");
                        personsList.add(postSnapshot.getValue(PersonEntity.class));
                    }

                    Log.d("ENTRO", "Lista de pèrsonas " + personsList.size());
                    emitter.onNext(personsList);
                    Log.d("ENTRO", "****************Completado queryPerson para:" + personsList.get(0).getName());
                    emitter.onComplete();
                }

                @Override
                public void onCancelled(DatabaseError databaseError) {
                    Log.d("ENTRO", "Entro onCancelled  queryPerson CloudImpl");
                    emitter.onError(new Throwable("Erro query person"));
                }
            });


        });
    }

    private void prueba(ObservableEmitter emitter) {

    }

    @Override
    public Observable<TravelEntity> addTravel(TravelEntity item) {

        return Observable.create(emitter -> {

            DatabaseReference reference = database.getReference(FirebaseReferences.TRAVELS_REFERENCE);
            String keyTravel = reference.push().getKey();
            item.setUid(keyTravel);

            reference.child(keyTravel).setValue(item, new DatabaseReference.CompletionListener() {

                @Override
                public void onComplete(DatabaseError databaseError, DatabaseReference databaseReference) {
                    if (databaseError == null) {


                        DatabaseReference referenceUser = database.getReference(FirebaseReferences.USERS_REFERENCE);
                        referenceUser.child(item.getUid_publisher()).addListenerForSingleValueEvent(new ValueEventListener() {
                            @Override
                            public void onDataChange(DataSnapshot dataSnapshot) {

                                PersonEntity personEntity = dataSnapshot.getValue(PersonEntity.class);
                                int travels = personEntity.getTravels() + 1;

                                referenceUser.child(item.getUid_publisher())
                                        .child(FirebaseReferences.USER_TRAVELS_REFERENCE)
                                        .setValue(travels);


                            }

                            @Override
                            public void onCancelled(DatabaseError databaseError) {
                                emitter.onError(new Throwable(databaseError.getMessage()));
                            }
                        });

                        emitter.onNext(item);
                        emitter.onComplete();
                    } else {
                        emitter.onError(new Throwable(databaseError.getMessage()));
                    }
                }
            });

        });

    }

    @Override
    public Observable<List<TravelEntity>> addTravel(List<TravelEntity> items) {


        return Observable.create(emitter -> {

            List<TravelEntity> listTravels = new ArrayList<>();

            for (TravelEntity travel :
                    items) {
                addTravel(travel);
                listTravels.add(travel);
            }

            Observable.just(listTravels);

        });


    }


    @Override
    public Observable<TravelEntity> updateTravel(TravelEntity travel) {

        return Observable.create(emitter -> {

            DatabaseReference reference = database.getReference(FirebaseReferences.TRAVELS_REFERENCE);

            reference.child(travel.getUid()).setValue(travel, new DatabaseReference.CompletionListener() {
                @Override
                public void onComplete(DatabaseError databaseError, DatabaseReference databaseReference) {

                    if (databaseError == null) {
                        emitter.onNext(travel);
                        emitter.onComplete();
                    } else {
                        emitter.onError(new Throwable(databaseError.getMessage()));
                    }

                }
            });

        });

    }


    @Override
    public Observable<TravelEntity> removeTravel(TravelEntity item) {


        throw new UnsupportedOperationException();
    }

    @Override
    public Observable<List<TravelEntity>> removeTravel(Specification specification) {
        throw new UnsupportedOperationException();
    }

    @Override
    public Observable<List<TravelEntity>> queryTravel(Specification specification) {


        final FirebaseSpecification firebaseSpecification = (FirebaseSpecification) specification;


        Query query = firebaseSpecification.toFirebaseQuery();


        Observable<TravelEntity> o = Observable.create(emitter -> {


            query.addListenerForSingleValueEvent(new ValueEventListener() {
                @Override
                public void onDataChange(DataSnapshot dataSnapshot) {


                    for (DataSnapshot postSnapshot : dataSnapshot.getChildren()) {
                        TravelEntity auxTravel = postSnapshot.getValue(TravelEntity.class);
                        Log.d("***ENTRO", "Entro emmiterOnNEx travel entity");
                        emitter.onNext(auxTravel);
                    }
                    Log.d("***ENTRO", "Entro onComplete travelEntity");
                    emitter.onComplete();

                }

                @Override
                public void onCancelled(DatabaseError databaseError) {
                    emitter.onError(new Throwable(databaseError.getMessage()));
                }


            });

        });


        return o.flatMap(
                s -> setPublisherInTravelObservable(s)
        ).toList().toObservable();

    }


    private ObservableSource<TravelEntity> setPublisherInTravelObservable(TravelEntity travelEntity) {

        return (emitter -> {


            DatabaseReference reference = database.getReference(FirebaseReferences.USERS_REFERENCE)
                    .child(travelEntity.getUid_publisher());

            reference.addListenerForSingleValueEvent(new ValueEventListener() {
                @Override
                public void onDataChange(DataSnapshot dataSnapshot) {

                    travelEntity.setPublisher(dataSnapshot.getValue(PersonEntity.class));

                    emitter.onNext(travelEntity);
                    emitter.onComplete();
                }

                @Override
                public void onCancelled(DatabaseError databaseError) {
                    Log.d("ENTRO", "Entro onCallded on EMitter setPublisher");
                    emitter.onError(new Throwable(databaseError.getMessage()));
                }
            });

        }


        );

    }

    @Override
    public Observable<List<TravelEntity>> queryTravelBySearch(SearchTravel searchTravel) {


        Query query = FirebaseDatabase.getInstance()
                .getReference(FirebaseReferences.TRAVELS_REFERENCE)
                .orderByChild(FirebaseReferences.TRAVELS_UID_REFERENCE);

        Observable<TravelEntity> o = Observable.create(emitter -> {

            query.addListenerForSingleValueEvent(new ValueEventListener() {
                @Override
                public void onDataChange(DataSnapshot dataSnapshot) {

                    for (DataSnapshot data :
                            dataSnapshot.getChildren()) {
                        TravelEntity travelEntity = data.getValue(TravelEntity.class);

                        if (travelEntity.getLocation_origin().equals(searchTravel.getFrom()) &&
                                travelEntity.getLocation_destination().equals(searchTravel.getTo()) &&
                                travelEntity.getDate_departure().equals(travelEntity.getDate_departure()) &&
                                travelEntity.getAvailable_space() >= searchTravel.getWeight()) {
                            emitter.onNext(travelEntity);
                        }


                    }

                    emitter.onComplete();


                }

                @Override
                public void onCancelled(DatabaseError databaseError) {
                    emitter.onError(new Throwable(databaseError.getMessage()));
                }
            });

        });

        return o.flatMap(
                s -> setPublisherInTravelObservable(s)
        ).toList().toObservable();
    }


    @Override
    public Observable<List<TravelEntity>> queryTravelsByPublisher(String uidPublisher) {

        Log.d("ENTRO", "ENtro queryTravelPublisher CloudImpl id de usuario: " + uidPublisher) ;
        Query query = database.getReference(FirebaseReferences.TRAVELS_REFERENCE)
                .orderByChild(FirebaseReferences.UID_PUBLISHER_REFERENCE)
                .equalTo(uidPublisher);

        Observable<TravelEntity> observable = Observable.create(emitter -> {

            query.addListenerForSingleValueEvent(new ValueEventListener() {
                @Override
                public void onDataChange(DataSnapshot dataSnapshot) {

                    Log.d("ENTRO", "ENtro queryTravelPublisher CloudImpl numero de viajes: " +dataSnapshot.getChildrenCount()) ;

                    for (DataSnapshot data :
                            dataSnapshot.getChildren()) {

                        TravelEntity travelEntity = data.getValue(TravelEntity.class);
                        emitter.onNext(travelEntity);
                    }

                    emitter.onComplete();


                }

                @Override
                public void onCancelled(DatabaseError databaseError) {
                    Log.d("ENTRO", "Entro onCacelled onError emisser");
                    emitter.onError(new Throwable(databaseError.getMessage()));
                }
            });

        });

        return observable.flatMap(
                s -> setPublisherInTravelObservable(s)
        ).toList().toObservable();


    }

    @Override
    public Observable<List<TravelEntity>> queryTravelsByApplicant(String uidApplicant) {
        Query query = database.getReference(FirebaseReferences.TRAVELS_REQUESTED_REFEENCE)
                .orderByChild(FirebaseReferences.UID_APPLICANT_REFERENCE)
                .equalTo(uidApplicant);

        Log.d("***ENTRO", "ENTRO QUERY TRAVEL APPLICANT  id applicant parametro: " + uidApplicant);


        Observable<RequestTravel> observableRequest = Observable.create(emitter -> {

            Log.d("***ENTRO", "Entro emmiter requestTravel");

            query.addListenerForSingleValueEvent(new ValueEventListener() {
                @Override
                public void onDataChange(DataSnapshot dataSnapshot) {

                    Log.d("***ENTRO", "Entro danaSnpashot requestTravel cound -- " + dataSnapshot.getChildrenCount());

                    for (DataSnapshot data :
                            dataSnapshot.getChildren()) {

                        RequestTravel requestTravel = data.getValue(RequestTravel.class);
                        Log.d("***ENTRO", "Entro emmiterOnNEx requestTravel");
                        emitter.onNext(requestTravel);
                    }

                    Log.d("***ENTRO", "Entro onComplete requestTravel");
                    emitter.onComplete();


                }

                @Override
                public void onCancelled(DatabaseError databaseError) {
                    emitter.onError(new Throwable(databaseError.getMessage()));
                }
            });

        });

        return observableRequest.flatMap(
                s -> queryTravel(new TravelByIdSpecification(s.getUid_travel()))
        );

    }


    @Override
    public Observable<TravelEntity> requestTravel(RequestTravel requestTravel) {


        Log.d("ENTRO", "Entro requestTravel CloudImpl");
        Log.d("ENTRO", "ENtro request travel uid publishjer " + requestTravel.getUid_publisher());
        Log.d("ENTRO", "ENtro request travel uid applicant " + requestTravel.getUid_applicant());
        Log.d("ENTRO", "ENtro request travel uid travek " + requestTravel.getUid_travel());


        return Observable.create(emitter -> {

            Log.d("ENTRO", "Entro emitter requestTravel CloudImpl");
            DatabaseReference referenceRequested = database.getReference(FirebaseReferences.TRAVELS_REQUESTED_REFEENCE);


            String keyRequesTravel = referenceRequested.push().getKey();

            referenceRequested.child(keyRequesTravel)
                    .setValue(requestTravel, new DatabaseReference.CompletionListener() {
                        @Override
                        public void onComplete(DatabaseError databaseError, DatabaseReference databaseReference) {


                            Log.d("ENTRO", "Entro onComplete requestTravel CloudImpl");

                            if (databaseError == null) {

                                Log.d("ENTRO", "Entro onComplete OK requestTravel CloudImpl");
                                DatabaseReference referenceTravel = database.getReference(FirebaseReferences.TRAVELS_REFERENCE);

                                referenceTravel.child(requestTravel.getUid_travel())
                                        .addListenerForSingleValueEvent(new ValueEventListener() {
                                            @Override
                                            public void onDataChange(DataSnapshot dataSnapshot) {
                                                TravelEntity travelEntity = dataSnapshot.getValue(TravelEntity.class);

                                                if (requestTravel.getSpace_requested() > travelEntity.getAvailable_space()) {
                                                    emitter.onError(new Throwable("Spacio no disponible"));

                                                } else {


                                                    DatabaseReference referencePerson = database.getReference(FirebaseReferences.USERS_REFERENCE);


                                                    referencePerson.child(requestTravel.getUid_applicant())
                                                            .addListenerForSingleValueEvent(new ValueEventListener() {
                                                                @Override
                                                                public void onDataChange(DataSnapshot dataSnapshot) {
                                                                    PersonEntity personEntity = dataSnapshot.getValue(PersonEntity.class);
                                                                    int travels = personEntity.getTravels() + 1;

                                                                    referencePerson.child(requestTravel.getUid_applicant())
                                                                            .child(FirebaseReferences.USER_TRAVELS_REFERENCE)
                                                                            .setValue(travels);

                                                                }

                                                                @Override
                                                                public void onCancelled(DatabaseError databaseError) {
                                                                    emitter.onError(new Throwable(databaseError.getMessage()));
                                                                }
                                                            });

                                                    double spaceAvailable = travelEntity.getAvailable_space() - requestTravel.getSpace_requested();


                                                    referenceTravel.child(requestTravel.getUid_travel())
                                                            .child(FirebaseReferences.TRAVEL_SPACE_AVAILABLE_REFERENCE)
                                                            .setValue(spaceAvailable);

                                                    emitter.onNext(travelEntity);
                                                    emitter.onComplete();

                                                }

                                            }

                                            @Override
                                            public void onCancelled(DatabaseError databaseError) {
                                                emitter.onError(new Throwable(databaseError.getMessage()));
                                            }
                                        });


                            } else {
                                Log.d("ENTRO", "Entro onComplete error requestTravel CloudImpl");
                                emitter.onError(new Throwable(databaseError.getMessage()));
                            }


                        }
                    });


        });

    }

}



