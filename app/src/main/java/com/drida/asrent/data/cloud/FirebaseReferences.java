package com.drida.asrent.data.cloud;

public class FirebaseReferences {

    public static final String USERS_REFERENCE = "users";
    public static final String TRAVELS_REFERENCE = "travels";
    public static final String TRAVELS_REQUESTED_REFEENCE = "travelsrequested";
    public static final String UID_APPLICANT_REFERENCE = "uid_applicant";
    public static final String UID_PUBLISHER_REFERENCE = "uid_publisher";
    public static final String USER_TRAVELS_REFERENCE = "travels";
    public static final String TRAVEL_SPACE_AVAILABLE_REFERENCE = "available_space";
    public static final String USER_UID_REFERENCE = "uid";
    public static final String TRAVELS_UID_REFERENCE = "uid";

}
