package com.drida.asrent.data.repository.datasource.core;

import com.drida.asrent.data.entity.AccountEntity;
import com.drida.asrent.data.entity.PersonEntity;
import com.drida.asrent.data.entity.TravelEntity;
import com.drida.asrent.data.specification.Specification;
import com.drida.asrent.domain.model.contact.Person;
import com.drida.asrent.domain.model.travel.RequestTravel;
import com.drida.asrent.domain.model.travel.SearchTravel;
import com.drida.asrent.domain.model.travel.Travel;
import com.google.firebase.auth.AuthCredential;

import java.util.List;

import io.reactivex.Observable;
import io.reactivex.Single;


public interface IDataSource {

    Observable<AccountEntity> logInUser(String email, String password);

    Observable<AccountEntity> logInUserFacebook(AuthCredential credential);

    Observable<AccountEntity> logOutUser();

    AccountEntity getUserCurrent();

    boolean isUserAuhtenticated();

    Observable<AccountEntity> registredUser(PersonEntity person);


    Observable<TravelEntity> addTravel(TravelEntity travel);

    Observable<List<TravelEntity>> addTravel(List<TravelEntity> travels);

    Observable<TravelEntity> updateTravel(TravelEntity travel);

    Observable<TravelEntity> removeTravel(TravelEntity travel);

    Observable<List<TravelEntity>> removeTravel(Specification specification);

    Observable<List<TravelEntity>> queryTravel(Specification specification);

    Observable<List<TravelEntity>> queryTravelBySearch(SearchTravel searchTravel);

    Observable<List<TravelEntity>> queryTravelsByApplicant(String uidApplicant);

    Observable<List<TravelEntity>> queryTravelsByPublisher(String uidPublisher);

    Observable<TravelEntity> requestTravel(RequestTravel requesTravel);

    Observable<PersonEntity> addPerson(PersonEntity person);

    Observable<List<PersonEntity>> addPerson(List<PersonEntity> person);

    Observable<PersonEntity> updatePerson(PersonEntity person);

    Observable<PersonEntity> removePerson(PersonEntity person);

    Observable<List<PersonEntity>> removePerson(Specification specification);

    Observable<List<PersonEntity>> queryPerson(Specification specification);






}
