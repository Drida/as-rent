package com.drida.asrent.utils;

import android.util.Patterns;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.regex.Pattern;

public class Validations {
    public static final Pattern PATTERN_EMAIL = Patterns.EMAIL_ADDRESS;
    public static final SimpleDateFormat FORMAT_DATE = new SimpleDateFormat("dd/MM/yyyy");
    public static final int LENGHT_PASSWORD = 6;

    public static boolean validateEmail(String email) {


        return PATTERN_EMAIL.matcher(email).matches();

    }

    public static boolean validatePassword(String password) {

        return password.length() < LENGHT_PASSWORD;

    }

    public static boolean validaDate(String date) {

        try {
            FORMAT_DATE.setLenient(false);
            FORMAT_DATE.parse(date);
        } catch (ParseException e) {
            return false;
        }
        return true;

    }

}
