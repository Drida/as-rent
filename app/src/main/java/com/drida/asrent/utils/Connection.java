package com.drida.asrent.utils;

import android.app.Activity;
import android.content.Context;
import android.net.ConnectivityManager;
import android.net.Network;
import android.net.NetworkInfo;
import android.util.Log;
import android.view.Gravity;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Toast;

import com.drida.asrent.R;

public class Connection {


    private static Toast toast;

    public static boolean isConnectionWithToast(Activity context) {


        ConnectivityManager connectivityManager = (ConnectivityManager) context.getSystemService(Context.CONNECTIVITY_SERVICE);
        NetworkInfo networkInfo = connectivityManager.getActiveNetworkInfo();

        if (networkInfo != null && networkInfo.isConnected()) {
            Log.d("ENTRO", "Entro en si hay conexcion");
            return true;
        } else {
            Log.d("ENTRO", "Entro en no hay conexcion");
            // No hay conexión a Internet en este momento

            if (toast != null) {
                toast.cancel();
            }

            toast = new Toast(context);
            //usamos cualquier layout como Toast
            View toast_layout = context.getLayoutInflater().inflate(R.layout.toast_no_connection, (ViewGroup) context.findViewById(R.id.contentView));
            toast.setView(toast_layout);
            toast.setDuration(Toast.LENGTH_LONG);
            toast.setGravity(Gravity.BOTTOM, 0, 0);
            toast.show();
            return false;
        }


    }

    public static boolean isConnection(Activity context) {


        ConnectivityManager connectivityManager = (ConnectivityManager) context.getSystemService(Context.CONNECTIVITY_SERVICE);
        NetworkInfo networkInfo = connectivityManager.getActiveNetworkInfo();

        if (networkInfo != null && networkInfo.isConnected()) {
            Log.d("ENTRO", "Entro en si hay conexcion");
            return true;
        } else {
            Log.d("ENTRO", "Entro en no hay conexcion");
            // No hay conexión a Internet en este momento



            return false;
        }


    }


}
