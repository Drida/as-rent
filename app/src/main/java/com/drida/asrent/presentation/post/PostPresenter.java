package com.drida.asrent.presentation.post;

import android.support.annotation.NonNull;
import android.util.EventLog;
import android.util.Log;

import com.drida.asrent.domain.model.account.Account;
import com.drida.asrent.domain.model.account.AccountManager;
import com.drida.asrent.domain.model.event.UpdatePublisherTravelsEvent;
import com.drida.asrent.domain.model.travel.Travel;
import com.drida.asrent.domain.model.travel.TravelProvider;
import com.drida.asrent.presentation.login.LoginView;
import com.hannesdorfmann.mosby3.mvp.MvpBasePresenter;

import org.greenrobot.eventbus.EventBus;

import javax.inject.Inject;

import io.reactivex.Scheduler;
import io.reactivex.android.schedulers.AndroidSchedulers;
import io.reactivex.observers.DisposableObserver;
import io.reactivex.schedulers.Schedulers;

public class PostPresenter extends MvpBasePresenter<PostView> {

    private TravelProvider travelProvider;
    private AccountManager accountManager;
    private EventBus eventBus;
    private DisposableObserver<Travel> subscription;

    @Inject
    public PostPresenter(TravelProvider travelProvider, AccountManager accountManager, EventBus eventBus) {
        this.accountManager = accountManager;
        this.eventBus = eventBus;
        this.travelProvider = travelProvider;
    }


    public void doPostTravel(Travel travel) {

        Log.d("ENTRO", "Entro doPostTravel PostPresenter");

        ifViewAttached(view -> view.showLoading());

        cancelSubscription();

        subscription = new DisposableObserver<Travel>() {
            @Override
            public void onNext(Travel travel) {
                //eventBus.post(new UpdatePublisherTravelsEvent());
            }

            @Override
            public void onError(Throwable e) {
                Log.d("ENTRO", "Entro onError PostPresenter");
                ifViewAttached(new ViewAction<PostView>() {
                    @Override
                    public void run(@NonNull PostView view) {
                        view.showError(e.getMessage());
                    }
                });
            }

            @Override
            public void onComplete() {
                Log.d("ENTRO", "Entro onComplete PostPresenter");
                ifViewAttached(new ViewAction<PostView>() {
                    @Override
                    public void run(@NonNull PostView view) {
                        view.postSuccesful();
                    }
                });
            }

        };


        travel.setIdPublisher(accountManager.getCurrentAccount().getIdUser());
        Log.d("ENTRO", "Entro posTrravel PostPresente con id de usuario: " + accountManager.getCurrentAccount().getIdUser());
        travelProvider.postTravel(travel)
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(subscription);

    }

    private void cancelSubscription() {

        if (subscription != null && !subscription.isDisposed()) {
            subscription.dispose();
        }

    }

    @Override
    public void attachView(PostView view) {
        super.attachView(view);

    }

    @Override
    public void detachView() {

        super.detachView();

        cancelSubscription();
    }


}
