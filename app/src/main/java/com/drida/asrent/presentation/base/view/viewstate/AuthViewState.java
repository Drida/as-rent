package com.drida.asrent.presentation.base.view.viewstate;

import com.drida.asrent.presentation.base.view.AuthView;
import com.hannesdorfmann.mosby3.mvp.viewstate.lce.LceViewState;

public interface AuthViewState<D, V extends AuthView<D>> extends LceViewState<D,V>{

    static final int SHOWING_AUTHENTICATION_REQUIRED = 2;

    void setShowingAuthenticationRequired();



}
