package com.drida.asrent.presentation.editprofile;

import com.drida.asrent.dagger.NavigationModule;
import com.drida.asrent.dagger.ShareAppComponent;
import com.drida.asrent.dagger.ShareModule;

import javax.inject.Singleton;

import dagger.Component;

@Singleton
@Component(
        modules = {ShareModule.class, NavigationModule.class},
        dependencies = ShareAppComponent.class
)
public interface EditProfileComponent {

    EditProfilePresenter presenter();

    void inject(EditProfileActivity activity);

}
