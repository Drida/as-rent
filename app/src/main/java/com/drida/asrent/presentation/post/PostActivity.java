package com.drida.asrent.presentation.post;


import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.design.widget.TextInputLayout;
import android.text.TextUtils;
import android.util.Log;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ProgressBar;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;

import com.drida.asrent.R;
import com.drida.asrent.dagger.NavigationModule;
import com.drida.asrent.ShareApplication;
import com.drida.asrent.domain.model.contact.Person;
import com.drida.asrent.domain.model.travel.Travel;
import com.drida.asrent.presentation.base.view.BaseViewStateActivity;
import com.drida.asrent.utils.Connection;
import com.drida.asrent.utils.Dates;
import com.drida.asrent.utils.Dialogs;
import com.drida.asrent.utils.Hours;
import com.drida.asrent.utils.IntentStarter;
import com.drida.asrent.utils.Weights;
import com.google.android.gms.common.api.Status;
import com.google.android.gms.location.places.Place;
import com.google.android.gms.location.places.ui.PlaceAutocomplete;

import java.util.Calendar;
import java.util.Date;

import javax.inject.Inject;

import butterknife.BindView;
import butterknife.OnClick;
import butterknife.OnItemSelected;

public class PostActivity extends BaseViewStateActivity<PostView, PostPresenter, PostViewState>
        implements PostView {


    private final int PLACE_AUTOCOMPLETE_REQUEST_CODE_LOCATION_ORIGIN = 1;
    private final int PLACE_AUTOCOMPLETE_REQUEST_CODE_LOCATION_DESTINATION = 2;

    @BindView(R.id.post_form)
    ViewGroup postForm;
    @BindView(R.id.location_origin)
    EditText locationOrigin;
    @BindView(R.id.location_destination)
    EditText locationDestination;
    @BindView(R.id.date_departure)
    EditText dateDeparture;
    @BindView(R.id.hour_departure)
    EditText hourDeparture;
    @BindView(R.id.date_arrival)
    EditText dateArrival;
    @BindView(R.id.hour_arrival)
    EditText hourArrival;
    @BindView(R.id.price)
    EditText price;
    @BindView(R.id.weight)
    EditText weight;
    @BindView(R.id.type_weight)
    Spinner typeWeight;
    @BindView(R.id.price_per_weight_simbol)
    TextView price_per_weight_simbol;
    @BindView(R.id.post_button)
    Button postButton;

    @BindView(R.id.input_location_origin)
    TextInputLayout inputLocationOrigin;
    @BindView(R.id.input_location_destination)
    TextInputLayout inputLocationDestination;
    @BindView(R.id.input_date_departure)
    TextInputLayout inputDateDeparture;
    @BindView(R.id.input_hour_departure)
    TextInputLayout inputHourDeparture;
    @BindView(R.id.input_date_arrival)
    TextInputLayout inputDateArrival;
    @BindView(R.id.input_hour_arrival)
    TextInputLayout inputHourArrival;
    @BindView(R.id.input_price)
    TextInputLayout inputPrice;
    @BindView(R.id.input_weight)
    TextInputLayout inputWeight;

    @BindView(R.id.progress_bar)
    ProgressBar progressBar;


    @Inject
    IntentStarter intentStarter;


    PostComponent postComponent;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        setContentView(R.layout.activity_post);


        loadDataInTypeWeightSpinner();

        setFormEnabled(true);
    }

    private void loadDataInTypeWeightSpinner() {

        ArrayAdapter<CharSequence> adapter = ArrayAdapter.createFromResource(this, R.array.types_weight, android.R.layout.simple_spinner_item);

        typeWeight.setAdapter(adapter);

    }

    @NonNull
    @Override
    public PostPresenter createPresenter() {
        return postComponent.presenter();
    }

    @OnClick(R.id.post_button)
    public void onPostClicked() {

        if (validateData() && Connection.isConnectionWithToast(this)) {

            Travel travelPost = new Travel();

            travelPost.setLocationOrigin(locationOrigin.getText().toString());
            travelPost.setLocationDestination(locationDestination.getText().toString());
            travelPost.setDateDeparture(Dates.transformDateBaseToDateString(dateDeparture.getText().toString()));
            travelPost.setHourDeparture(hourDeparture.getText().toString());
            travelPost.setDateArrival(Dates.transformDateBaseToDateString(hourArrival.getText().toString()));
            travelPost.setHourArrival(hourArrival.getText().toString());
            travelPost.setPriceWeight(Double.parseDouble(price.getText().toString()));
            travelPost.setTypeWeight(typeWeight.getSelectedItem().toString());
            double weightTotal = 0;
            if (typeWeight.getSelectedItemPosition() == 1) {
                weightTotal = Weights.transformKilosToGram(Double.parseDouble(weight.getText().toString()));
            } else if (typeWeight.getSelectedItemPosition() == 0) {
                weightTotal = Double.parseDouble(weight.getText().toString());
            }
            travelPost.setAvailableSpace(weightTotal);

            Log.d("ENTRO", "ENTRO onPostClicked()");
            presenter.doPostTravel(travelPost);

        }

    }

    @OnItemSelected(R.id.type_weight)
    public void onTypeWeightSelected() {

        price_per_weight_simbol.setText(getString(R.string.text_euros) + "/" + typeWeight.getSelectedItem());

    }


    @OnClick(R.id.location_origin)
    public void onLocationOriginClicked() {

        intentStarter.showAutocompletePlace(this, PLACE_AUTOCOMPLETE_REQUEST_CODE_LOCATION_ORIGIN);

    }

    @OnClick(R.id.location_destination)
    public void onLocationDestinationClicked() {

        intentStarter.showAutocompletePlace(this, PLACE_AUTOCOMPLETE_REQUEST_CODE_LOCATION_DESTINATION);

    }

    @OnClick(R.id.date_departure)
    public void onDateDepartureClicked() {

        Dates.showDatePickerDialog(this, dateDeparture);

    }

    @OnClick(R.id.hour_departure)
    public void onHourDepartureClicked() {

        Hours.showTimePickerDialog(this, hourDeparture);

    }

    @OnClick(R.id.date_arrival)
    public void onDateArrivalClicked() {

        Dates.showDatePickerDialog(this, dateArrival);

    }

    @OnClick(R.id.hour_arrival)
    public void onHourArrivalClicked() {

        Hours.showTimePickerDialog(this, hourArrival);

    }


    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {


        if (requestCode == PLACE_AUTOCOMPLETE_REQUEST_CODE_LOCATION_ORIGIN) {

            if (resultCode == RESULT_OK) {
                Place place = PlaceAutocomplete.getPlace(this, data);
                locationOrigin.setText(place.getName());
            } else if (resultCode == PlaceAutocomplete.RESULT_ERROR) {
                Status status = PlaceAutocomplete.getStatus(this, data);

                Log.i("ERROR", "Entro Result Error");
            } else if (resultCode == RESULT_CANCELED) {
                Log.d("ERROR", "Entro RESULT CANCELED");
            }

        }

        if (requestCode == PLACE_AUTOCOMPLETE_REQUEST_CODE_LOCATION_DESTINATION) {

            if (resultCode == RESULT_OK) {
                Place place = PlaceAutocomplete.getPlace(this, data);
                locationDestination.setText(place.getName());
            } else if (resultCode == PlaceAutocomplete.RESULT_ERROR) {
                Status status = PlaceAutocomplete.getStatus(this, data);

                Log.i("ERROR", "Entro Result Error");
            } else if (resultCode == RESULT_CANCELED) {
                Log.d("ERROR", "Entro RESULT CANCELED");
            }

        }

    }


    @Override
    protected void injectDependencies() {
        postComponent = DaggerPostComponent.builder()
                .shareAppComponent(ShareApplication.getShareComponents())
                .navigationModule(new NavigationModule())
                .build();

        postComponent.inject(this);
    }


    private boolean validateData() {

        boolean valid = true;


        if (TextUtils.isEmpty(locationOrigin.getText())) {
            inputLocationOrigin.setError(getString(R.string.error_field_required));
            valid = false;
        } else {
            inputLocationOrigin.setError(null);
        }

        if (TextUtils.isEmpty(locationDestination.getText())) {
            inputLocationDestination.setError(getString(R.string.error_field_required));
            valid = false;
        } else {
            inputLocationDestination.setError(null);
        }

        if (TextUtils.isEmpty(dateDeparture.getText())) {
            inputDateDeparture.setError(getString(R.string.error_field_required));
            valid = false;

        } else {

            Calendar calendar = Dates.convertStringBaseToCalendar(dateDeparture.getText().toString());

            if (calendar.before(Dates.getDateCurrentBase())) {
                inputDateDeparture.setError(getString(R.string.error_date_departure));
                valid = false;
            } else {
                inputDateDeparture.setError(null);

            }
        }

        if (TextUtils.isEmpty(hourDeparture.getText())) {
            inputHourDeparture.setError(getString(R.string.error_field_required));
            valid = false;
        } else {

            Calendar calendar = Dates.convertStringBaseToCalendar(dateDeparture.getText().toString());
            Date hourAux = Hours.converHourStringToDate(hourDeparture.getText().toString());


            if (calendar.equals(Dates.getDateCurrentBase())) {
                if (hourAux.compareTo(Hours.getHourCurrent()) < 0) {
                    inputHourDeparture.setError(getString(R.string.error_hour_departure));
                    valid = false;
                } else {
                    inputHourDeparture.setError(null);
                }
            } else {
                inputHourDeparture.setError(null);
            }


        }


        if (TextUtils.isEmpty(dateArrival.getText())) {
            inputDateArrival.setError(getString(R.string.error_field_required));
            valid = false;

        } else {

            Calendar calendarDateArrival = Dates.convertStringBaseToCalendar(dateArrival.getText().toString());
            Calendar calendarDateDeparture = Dates.convertStringBaseToCalendar(dateDeparture.getText().toString());


            if (calendarDateArrival.before(calendarDateDeparture)) {
                inputDateArrival.setError(getString(R.string.error_date_arrival));
                valid = false;
            } else {
                inputDateArrival.setError(null);

            }
        }

        if (TextUtils.isEmpty(hourArrival.getText())) {
            inputHourArrival.setError(getString(R.string.error_field_required));
            valid = false;
        } else {

            Calendar calendarDateArrival = Dates.convertStringBaseToCalendar(dateArrival.getText().toString());
            Calendar calendarDateDeparture = Dates.convertStringBaseToCalendar(dateDeparture.getText().toString());
            Date hourArrivalAux = Hours.converHourStringToDate(hourArrival.getText().toString());
            Date hourDepartureAux = Hours.converHourStringToDate(hourDeparture.getText().toString());

            if (calendarDateArrival.equals(calendarDateDeparture)) {

                if (hourArrivalAux.compareTo(hourDepartureAux) < 0) {
                    inputHourArrival.setError(getString(R.string.error_hour_arrival));
                    valid = false;
                } else {
                    inputHourArrival.setError(null);
                }
            } else {
                inputHourArrival.setError(null);
            }

        }


        if (TextUtils.isEmpty(price.getText())) {
            inputPrice.setError(getString(R.string.error_field_required));
            valid = false;
        } else {
            inputPrice.setError(null);
        }

        if (TextUtils.isEmpty(weight.getText())) {
            inputWeight.setError(getString(R.string.error_field_required));
            valid = false;
        } else {
            inputWeight.setError(null);
            if (weight.getText().toString().equals("0")) {
                inputWeight.setError(getString(R.string.error_invalid_weight));
                valid = false;
            }

        }


        return valid;

    }

    private void setFormEnabled(boolean enabled) {

        postForm.setEnabled(enabled);
        locationOrigin.setEnabled(enabled);
        locationDestination.setEnabled(enabled);
        dateDeparture.setEnabled(enabled);
        dateArrival.setEnabled(enabled);
        dateArrival.setEnabled(enabled);
        hourArrival.setEnabled(enabled);
        price.setEnabled(enabled);
        weight.setEnabled(enabled);
        typeWeight.setEnabled(enabled);
        postButton.setEnabled(enabled);

    }

    @Override
    public void showPostForm() {

        viewState.setShowPostForm();

        progressBar.setVisibility(View.GONE);

        setFormEnabled(true);

    }

    @Override
    public void showError(String messageError) {

        viewState.setShowError(messageError);

        progressBar.setVisibility(View.GONE);
        setFormEnabled(true);


        Dialogs.showDialogSimple(this, getString(R.string.text_error), messageError);

    }

    @Override
    public void showLoading() {

        progressBar.setVisibility(View.VISIBLE);
        viewState.setShowLoading();
        setFormEnabled(false);

    }

    @Override
    public void postSuccesful() {

        progressBar.setVisibility(View.GONE);
        Toast.makeText(this, getString(R.string.ok_post_travel), Toast.LENGTH_SHORT).show();
        this.finish();

    }

    @NonNull
    @Override
    public PostViewState createViewState() {
        return new PostViewState();
    }

    @Override
    public void onNewViewStateInstance() {
        showPostForm();
    }
}
