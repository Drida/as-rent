package com.drida.asrent.presentation.search;

import com.drida.asrent.dagger.NavigationModule;
import com.drida.asrent.dagger.ShareAppComponent;

import javax.inject.Singleton;

import dagger.Component;

@Singleton
@Component(
        modules = NavigationModule.class,
        dependencies = ShareAppComponent.class
)
public interface SearchComponent {

    void inject(SearchActivity activity);
}
