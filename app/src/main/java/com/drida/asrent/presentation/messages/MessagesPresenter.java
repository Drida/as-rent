package com.drida.asrent.presentation.messages;


import com.drida.asrent.domain.model.travel.TravelProvider;
import com.hannesdorfmann.mosby3.mvp.MvpBasePresenter;

import org.greenrobot.eventbus.EventBus;

import javax.inject.Inject;

public class MessagesPresenter extends MvpBasePresenter<MessagesView>{


    TravelProvider travelProvider;
    EventBus eventBus;

    @Inject
    public MessagesPresenter(TravelProvider travelProvider, EventBus eventBus) {
        this.travelProvider = travelProvider;
        this.eventBus = eventBus;
    }
}
