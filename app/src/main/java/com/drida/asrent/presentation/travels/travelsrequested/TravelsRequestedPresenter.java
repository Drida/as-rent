package com.drida.asrent.presentation.travels.travelsrequested;



import com.drida.asrent.domain.model.account.AccountManager;
import com.drida.asrent.domain.model.event.UpdatePublisherTravelsEvent;
import com.drida.asrent.domain.model.event.UpdateRequestedTravelsEvent;
import com.drida.asrent.domain.model.travel.Travel;
import com.drida.asrent.domain.model.travel.TravelProvider;
import com.drida.asrent.presentation.base.presenter.BaseRxTravelPresenter;

import org.greenrobot.eventbus.EventBus;
import org.greenrobot.eventbus.Subscribe;
import org.greenrobot.eventbus.ThreadMode;

import java.util.List;

import javax.inject.Inject;

public class TravelsRequestedPresenter extends BaseRxTravelPresenter<TravelsRequestedView, List<Travel>> {

    private AccountManager accountManager;

    @Inject
    public TravelsRequestedPresenter(TravelProvider travelProvider, AccountManager accountManager, EventBus eventBus) {
        super(travelProvider, eventBus);
        this.accountManager = accountManager;
    }

    public void load(boolean pullToRefresh){


        subscribe(travelProvider.getTravelsOfApplicant(accountManager.getCurrentAccount().getIdUser()), pullToRefresh);

    }


    @Subscribe(threadMode = ThreadMode.MAIN_ORDERED)
    public void onEventMainThread(UpdateRequestedTravelsEvent event) {


        ifViewAttached(true, view -> view.loadData(true));
    }

}
