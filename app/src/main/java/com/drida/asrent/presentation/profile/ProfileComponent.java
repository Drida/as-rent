package com.drida.asrent.presentation.profile;


import com.drida.asrent.dagger.NavigationModule;
import com.drida.asrent.dagger.ShareAppComponent;
import com.drida.asrent.dagger.ShareModule;
import com.drida.asrent.presentation.profile.account.ProfileAccountFragment;
import com.drida.asrent.presentation.profile.account.ProfileAccountPresenter;
import com.drida.asrent.presentation.profile.deatails.ProfileDetailsFragment;

import javax.inject.Singleton;

import dagger.Component;

@Singleton
@Component (
        modules = {
                ShareModule.class, NavigationModule.class
        }, dependencies = ShareAppComponent.class
)
public interface ProfileComponent {


        ProfilePresenter presenter();
        void inject(ProfileFragment fragment);
        void inject(ProfileDetailsFragment profileDetailsFragment);
        ProfileAccountPresenter presenterAccount();
        void inject(ProfileAccountFragment profileAccountFragment);

}
