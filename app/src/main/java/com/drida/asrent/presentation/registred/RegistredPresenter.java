package com.drida.asrent.presentation.registred;


import android.support.annotation.NonNull;

import com.drida.asrent.domain.model.account.Account;
import com.drida.asrent.domain.model.account.AccountManager;
import com.drida.asrent.domain.model.contact.Person;

import com.drida.asrent.domain.model.event.LoginSuccessfulEvent;
import com.hannesdorfmann.mosby3.mvp.MvpBasePresenter;


import org.greenrobot.eventbus.EventBus;

import javax.inject.Inject;

import io.reactivex.android.schedulers.AndroidSchedulers;
import io.reactivex.observers.DisposableObserver;
import io.reactivex.schedulers.Schedulers;


public class RegistredPresenter extends MvpBasePresenter<RegistredView> {

    private AccountManager accountManager;
    private EventBus eventBus;
    private DisposableObserver<Account> subscription;

    @Inject
    public RegistredPresenter(AccountManager accountManager, EventBus eventBus) {

        this.accountManager = accountManager;
        this.eventBus = eventBus;

    }

    public void doRegistred(Person person) {

        ifViewAttached(view -> view.showLoading());

        cancelSubscription();

        subscription = new DisposableObserver<Account>() {
            @Override
            public void onNext(Account account) {
                ifViewAttached(view -> new Thread(() -> {
                    try {
                        Thread.sleep(300);
                    } catch (InterruptedException e) {
                        e.printStackTrace();
                    }
                    eventBus.post(new LoginSuccessfulEvent(account));

                }).start());
            }

            @Override
            public void onError(Throwable e) {
                ifViewAttached(view -> view.showError());
            }

            @Override
            public void onComplete() {
                ifViewAttached(view -> view.registredSuccesfull());
            }
        };


        accountManager.doRegistred(person)
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(subscription);


    }

    private void cancelSubscription() {

        if (subscription != null && !subscription.isDisposed()) {
              subscription.dispose();
         }

    }

    @Override
    public void attachView(RegistredView view) {
        super.attachView(view);
    }

    @Override
    public void detachView() {

        super.detachView();

        cancelSubscription();
    }


}
