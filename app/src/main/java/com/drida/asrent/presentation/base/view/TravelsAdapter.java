package com.drida.asrent.presentation.base.view;

import android.content.Context;
import android.util.Log;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;

import com.drida.asrent.R;

import com.drida.asrent.domain.model.travel.Travel;
import com.drida.asrent.utils.Dates;
import com.drida.asrent.utils.Prices;
import com.hannesdorfmann.annotatedadapter.annotation.ViewField;
import com.hannesdorfmann.annotatedadapter.annotation.ViewType;

import java.util.ArrayList;
import java.util.List;

public class TravelsAdapter extends ListAdapter<List<Travel>> implements TravelsAdapterBinder {


    public interface TravelClickedListener {
        public void onTravelClicked(TravelsAdapterHolders.TravelViewHolder vh, Travel travel);
    }

    @ViewType(
            layout = R.layout.list_travel_item,
            views = {
                    @ViewField(id = R.id.date, name = "date", type = TextView.class),
                    @ViewField(id = R.id.hour_start, name = "hour_start", type = TextView.class),
                    @ViewField(id = R.id.hour_finish, name = "hour_finish", type = TextView.class),
                    @ViewField(id = R.id.place_start, name = "place_start", type = TextView.class),
                    @ViewField(id = R.id.place_finish, name = "place_finish", type = TextView.class),
                    @ViewField(id = R.id.price, name = "price", type = TextView.class),
                    @ViewField(id = R.id.image_publisher, name = "image_publisher", type = ImageView.class),
                    @ViewField(id = R.id.name_publisher, name = "name_publisher", type = TextView.class)


            }) public final int travel = 0;


    private TravelClickedListener clickedListener;



    public TravelsAdapter(Context context, TravelClickedListener clickedListener) {
        super(context);
        this.clickedListener = clickedListener;

    }

    @Override
    public void bindViewHolder(final TravelsAdapterHolders.TravelViewHolder vh, int position) {

        final Travel travel = items.get(position);


        vh.date.setText(Dates.joinDates(travel.getDateDeparture(), travel.getDateArrival()));
        vh.hour_start.setText(travel.getHourDeparture());
        vh.hour_finish.setText(travel.getHourArrival());
        vh.place_start.setText(travel.getLocationOrigin());
        vh.place_finish.setText(travel.getLocationDestination());
        vh.name_publisher.setText(travel.getPublisher().getName());
        vh.image_publisher.setImageResource(R.drawable.ic_profile_black_24dp);
        vh.price.setText(Prices.transformToEuros(travel.getPriceWeight()) + "/" + travel.getTypeWeight());

        vh.itemView.setOnClickListener(view -> clickedListener.onTravelClicked(vh, travel));

    }

    @Override
    public void setItems(List<Travel> items) {

        List<Travel> itemsValidate = new ArrayList<>();
        for (Travel aux :
                items){
            if (Dates.isDateAvailable(aux.getDateDeparture(), aux.getHourDeparture()))
                itemsValidate.add(aux);
        }
        super.setItems(itemsValidate);
    }
}
