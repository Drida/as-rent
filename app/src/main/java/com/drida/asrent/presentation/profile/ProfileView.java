package com.drida.asrent.presentation.profile;

import com.drida.asrent.domain.model.contact.Person;

import com.drida.asrent.presentation.base.view.AuthView;
import com.hannesdorfmann.mosby3.mvp.lce.MvpLceView;

import java.util.List;

public interface ProfileView extends AuthView<Person> {



}
