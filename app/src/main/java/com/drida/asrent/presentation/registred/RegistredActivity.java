package com.drida.asrent.presentation.registred;

import android.os.Bundle;
import android.support.annotation.Nullable;

import com.drida.asrent.R;
import com.drida.asrent.presentation.base.view.BaseActivity;


public class RegistredActivity extends BaseActivity {

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        setContentView(R.layout.activity_registred);



        if(savedInstanceState == null){

            getSupportFragmentManager().beginTransaction().replace(R.id.fragmentContainer, new RegistredFragment()).commit();

        }
    }
}
