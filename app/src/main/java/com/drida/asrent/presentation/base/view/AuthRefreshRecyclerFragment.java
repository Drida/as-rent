package com.drida.asrent.presentation.base.view;

import android.os.Bundle;
import android.os.Parcelable;
import android.support.annotation.Nullable;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.View;


import com.drida.asrent.R;




import com.drida.asrent.presentation.base.view.viewstate.AuthCastedArrayListViewState;
import com.drida.asrent.presentation.base.view.viewstate.AuthViewState;
import com.hannesdorfmann.mosby3.mvp.MvpPresenter;

import java.util.List;

public abstract class AuthRefreshRecyclerFragment<M extends List<? extends Parcelable>, V extends AuthView<M>, P extends MvpPresenter<V>>
        extends AuthRefreshFragment<M,V,P>{

    protected View emptyView;
    protected RecyclerView recyclerView;
    protected ListAdapter<M> adapter;


    protected abstract ListAdapter<M> createAdapter();

    @Override
    public void onViewCreated(View view, @Nullable Bundle savedIntanceState) {
        super.onViewCreated(view, savedIntanceState);

        emptyView = view.findViewById(R.id.emptyView);
        recyclerView  = (RecyclerView) view.findViewById(R.id.recyclerView);
        adapter = createAdapter();
        recyclerView.setLayoutManager(new LinearLayoutManager(getActivity()));
        recyclerView.setAdapter(adapter);
    }


    @Override
    public void setData(M data) {
       adapter.setItems(data);
       adapter.notifyDataSetChanged();
    }


    @Override
    public AuthViewState<M, V> createViewState() {
        return new AuthCastedArrayListViewState<>();
    }

    @Override
    public M getData() {
        return adapter.getItems();
    }

    @Override
    public void showContent() {

        if(adapter.getItemCount() == 0){
            emptyView.setVisibility(View.VISIBLE);
        }else {
            emptyView.setVisibility(View.GONE);
        }

        super.showContent();
    }

    @Override
    public void showError(Throwable e, boolean pullToRefresh) {
        super.showError(e, pullToRefresh);
        if (!pullToRefresh) {
           emptyView.setVisibility(View.GONE);
        }
    }

    @Override
    public void showLoading(boolean pullToRefresh) {
        super.showLoading(pullToRefresh);
        if (!pullToRefresh) {
            emptyView.setVisibility(View.GONE);
        }
    }
}
