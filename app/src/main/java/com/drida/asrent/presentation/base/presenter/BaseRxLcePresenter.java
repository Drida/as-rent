
package com.drida.asrent.presentation.base.presenter;


import android.support.annotation.NonNull;
import android.util.Log;

import com.drida.asrent.utils.Connection;
import com.hannesdorfmann.mosby3.mvp.MvpBasePresenter;
import com.hannesdorfmann.mosby3.mvp.MvpPresenter;
import com.hannesdorfmann.mosby3.mvp.lce.MvpLceView;

import io.reactivex.Observable;
import io.reactivex.android.schedulers.AndroidSchedulers;
import io.reactivex.observers.DisposableObserver;
import io.reactivex.schedulers.Schedulers;


public abstract class BaseRxLcePresenter<V extends MvpLceView<M>, M>
        extends MvpBasePresenter<V>
        implements MvpPresenter<V> {

    protected DisposableObserver<M> subscription;

    protected void unsubscribe() {
        if (subscription != null && !subscription.isDisposed()) {
            subscription.dispose();
        }

        subscription = null;
    }

    public void subscribe(Observable<M> observable, final boolean pullToRefresh) {


        ifViewAttached(view -> view.showLoading(pullToRefresh));

        unsubscribe();

        subscription = new DisposableObserver<M>() {
            private boolean ptr = pullToRefresh;

            @Override
            public void onComplete() {
                BaseRxLcePresenter.this.onCompleted();
            }

            @Override
            public void onError(Throwable e) {

                Log.d("ENTRO", "ENtro on Error BaseRxLCePresenter :" + e.getStackTrace());
                BaseRxLcePresenter.this.onError(e, ptr);
            }

            @Override
            public void onNext(M m) {
                BaseRxLcePresenter.this.onNext(m);
            }
        };

        observable.subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(subscription);
    }


    protected void onCompleted() {
        ifViewAttached(view -> {

            view.showContent();
        });

        unsubscribe();
    }


    protected void onError(final Throwable e, final boolean pullToRefresh) {

        ifViewAttached(view -> view.showError(e, pullToRefresh));

        unsubscribe();
    }


    protected void onNext(final M data) {

        ifViewAttached(view -> view.setData(data));

    }


    @Override
    public void detachView() {
        unsubscribe();
    }


}
