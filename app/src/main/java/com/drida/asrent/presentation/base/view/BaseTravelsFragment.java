package com.drida.asrent.presentation.base.view;


import com.drida.asrent.R;


import com.drida.asrent.domain.model.travel.Travel;
import com.drida.asrent.presentation.base.presenter.BaseRxTravelPresenter;
import com.drida.asrent.utils.IntentStarter;

import java.util.List;

import javax.inject.Inject;

public abstract class BaseTravelsFragment<V extends BaseTravelView<List<Travel>>, P extends BaseRxTravelPresenter<V, List<Travel>>>
    extends AuthRefreshRecyclerFragment<List<Travel>, V, P>
    implements BaseTravelView<List<Travel>>, TravelsAdapter.TravelClickedListener {

    @Inject
    IntentStarter intentStarter;

    @Override
    protected int getLayoutRes() {
        return R.layout.fragment_travels_base;
    }


    @Override
    protected ListAdapter<List<Travel>> createAdapter() {
        return new TravelsAdapter(getActivity(), this);
    }

    @Override
    public void onTravelClicked(TravelsAdapterHolders.TravelViewHolder vh, Travel travel) {

        intentStarter.showTravelDetails(getActivity(),travel);

    }
}
