package com.drida.asrent.presentation.requesttrip;

import com.hannesdorfmann.mosby3.mvp.viewstate.ViewState;

public class RequestTripViewState implements ViewState<RequestTripView> {

    String errorMessage;
    double spaceAvailable;

    final int STATE_SHOW_REQUEST_TRIP_FORM = 0;
    final int STATE_SHOW_LOADING = 1;
    final int STATE_SHOW_ERROR = 2;
    final int STATE_SHOW_UPDATE_SPACE = 3;

    int state = STATE_SHOW_REQUEST_TRIP_FORM;

    public void setShowRequestTripForm() {
        state = STATE_SHOW_REQUEST_TRIP_FORM;
    }

    public void setShowError(String errorMessage) {
        state = STATE_SHOW_ERROR;
        this.errorMessage = errorMessage;
    }

    public void setShowUpdateSpaceAvailable(double space) {

        state = STATE_SHOW_UPDATE_SPACE;
        spaceAvailable = space;
    }

    public void setShowLoading() {
        state = STATE_SHOW_LOADING;
    }


    @Override
    public void apply(RequestTripView view, boolean retained) {

        switch (state) {
            case STATE_SHOW_LOADING:
                view.showLoading();
                break;

            case STATE_SHOW_ERROR:
                view.showError(errorMessage);
                break;

            case STATE_SHOW_REQUEST_TRIP_FORM:
                view.showRequestTripForm();
                break;
            case STATE_SHOW_UPDATE_SPACE:
                view.updateSpaceAvailable(spaceAvailable);
                break;
        }
    }


}

