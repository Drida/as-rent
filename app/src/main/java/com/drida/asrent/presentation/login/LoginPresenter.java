package com.drida.asrent.presentation.login;


import android.support.annotation.NonNull;
import android.util.Log;

import com.drida.asrent.domain.model.account.Account;
import com.drida.asrent.domain.model.account.AccountManager;
import com.drida.asrent.domain.model.account.AuthCredentials;
import com.drida.asrent.domain.model.event.LoginSuccessfulEvent;


import com.facebook.AccessToken;
import com.hannesdorfmann.mosby3.mvp.MvpBasePresenter;


import org.greenrobot.eventbus.EventBus;

import javax.inject.Inject;

import io.reactivex.android.schedulers.AndroidSchedulers;
import io.reactivex.observers.DisposableObserver;
import io.reactivex.schedulers.Schedulers;


public class LoginPresenter extends MvpBasePresenter<LoginView> {

    private AccountManager accountManager;
    private EventBus eventBus;
    private DisposableObserver<Account> subscription;

    @Inject
    public LoginPresenter(AccountManager accountManager, EventBus eventBus) {

        this.accountManager = accountManager;
        this.eventBus = eventBus;

    }


    public void doLogin(AuthCredentials credentials) {


        ifViewAttached(view -> view.showLoading());

        cancelSubscription();

        subscription = new DisposableObserver<Account>() {
            @Override
            public void onNext(Account account) {
                ifViewAttached(view -> new Thread(() -> {
                    try {
                        Thread.sleep(300);
                    } catch (InterruptedException e) {
                        e.printStackTrace();
                    }
                    eventBus.post(new LoginSuccessfulEvent(account));

                }).start());
            }

            @Override
            public void onError(Throwable e) {

                ifViewAttached(view -> view.showError());
            }

            @Override
            public void onComplete() {
                ifViewAttached(view -> view.loginSuccesful());
            }
        };


        accountManager.doLogin(credentials)
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(subscription);
    }

    public void doLoginFacebook(AccessToken accessToken) {

        ifViewAttached(new ViewAction<LoginView>() {
            @Override
            public void run(@NonNull LoginView view) {
                view.showLoading();
            }
        });

        cancelSubscription();

        subscription = new DisposableObserver<Account>() {
            @Override
            public void onNext(Account account) {
                ifViewAttached(new ViewAction<LoginView>() {
                    @Override
                    public void run(@NonNull LoginView view) {
                        eventBus.post(new LoginSuccessfulEvent(account));
                    }
                });
            }

            @Override
            public void onError(Throwable e) {
                ifViewAttached(new ViewAction<LoginView>() {
                    @Override
                    public void run(@NonNull LoginView view) {
                        view.showError();
                    }
                });
            }

            @Override
            public void onComplete() {
                ifViewAttached(new ViewAction<LoginView>() {
                    @Override
                    public void run(@NonNull LoginView view) {
                        view.loginSuccesful();
                    }
                });
            }
        };
        accountManager.doLoginFacebook(accessToken)
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(subscription);

    }


    private void cancelSubscription() {

        if (subscription != null && !subscription.isDisposed()) {
            subscription.dispose();
        }

    }

    @Override
    public void attachView(LoginView view) {
        super.attachView(view);
        Log.d("ENTRO", "ENtro attachView LoginPResenter");
    }

    @Override
    public void detachView() {

        super.detachView();

        cancelSubscription();
    }

}
