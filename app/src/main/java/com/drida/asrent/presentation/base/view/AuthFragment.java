package com.drida.asrent.presentation.base.view;

import android.os.Bundle;
import android.support.annotation.LayoutRes;
import android.support.annotation.Nullable;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;


import com.drida.asrent.presentation.base.view.viewstate.AuthViewState;
import com.drida.asrent.presentation.login.LoginFragment;
import com.drida.asrent.utils.IntentStarter;
import com.drida.asrent.R;

import com.hannesdorfmann.fragmentargs.FragmentArgs;
import com.hannesdorfmann.mosby3.mvp.MvpPresenter;

import javax.inject.Inject;

public abstract class AuthFragment <AV extends View, M, V extends AuthView<M>, P extends MvpPresenter<V>>
        extends BaseLceFragment<AV, M, V, P> implements AuthView<M>{


    protected View authView;
    @Inject IntentStarter intentStarter;

    @Override
    public void onCreate(Bundle savedInstanceState){

        super.onCreate(savedInstanceState);

        FragmentArgs.inject(this);
    }


    @LayoutRes
    protected abstract int getLayoutRes();

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container,
                             @Nullable Bundle savedInstanceState) {
        return inflater.inflate(getLayoutRes(), container, false);
    }

    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState){

        super.onViewCreated(view, savedInstanceState);

        authView = view.findViewById(R.id.authView);

        authView.setOnClickListener(new View.OnClickListener() {
            @Override public void onClick(View v) {
                onAuthViewClicked();
            }
        });


    }

    protected void onAuthViewClicked(){
        intentStarter.showAuthentication(getActivity());
    }

    @Override
    protected String getErrorMessage(Throwable e, boolean pullToRefresh){
        if (pullToRefresh) {
            return getString(R.string.text_error_ocurred);
        } else {
            return getString(R.string.text_error_ocurred_retry);
        }
    }

    @Override
    public void showAuthenticationRequired() {


        AuthViewState vs = (AuthViewState)viewState;
        vs.setShowingAuthenticationRequired();


        loadingView.setVisibility(View.GONE);
        errorView.setVisibility(View.GONE);
        contentView.setVisibility(View.GONE);
        authView.setVisibility(View.VISIBLE);

    }

    @Override
    public void showContent() {
        super.showContent();
        authView.setVisibility(View.GONE);

    }

    @Override
    public void showError(Throwable e, boolean pullToRefresh) {
        super.showError(e, pullToRefresh);
        authView.setVisibility(View.GONE);

    }

    @Override
    public void showLoading(boolean pullToRefresh) {
        super.showLoading(pullToRefresh);

        authView.setVisibility(View.GONE);
    }

    @Override
    public abstract AuthViewState<M, V> createViewState();



}
