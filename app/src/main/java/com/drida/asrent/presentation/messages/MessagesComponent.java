package com.drida.asrent.presentation.messages;

import com.drida.asrent.dagger.ShareAppComponent;
import com.drida.asrent.dagger.ShareModule;

import javax.inject.Singleton;

import dagger.Component;
@Singleton
@Component(

        modules = {ShareModule.class},
        dependencies = ShareAppComponent.class

)
public interface MessagesComponent {

    //MessagesPresenter presenter();
    void inject(MessagesFragment fragment);

}
