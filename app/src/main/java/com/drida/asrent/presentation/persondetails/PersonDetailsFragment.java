package com.drida.asrent.presentation.persondetails;

import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.view.View;
import android.widget.EditText;
import android.widget.TextView;

import com.drida.asrent.R;
import com.drida.asrent.ShareApplication;
import com.drida.asrent.domain.model.contact.Person;
import com.drida.asrent.presentation.base.view.BaseLceFragment;
import com.drida.asrent.utils.Dates;
import com.hannesdorfmann.fragmentargs.annotation.Arg;
import com.hannesdorfmann.fragmentargs.annotation.FragmentWithArgs;
import com.hannesdorfmann.mosby3.mvp.viewstate.lce.LceViewState;
import com.hannesdorfmann.mosby3.mvp.viewstate.lce.data.ParcelableDataLceViewState;

import butterknife.BindView;
import de.hdodenhof.circleimageview.CircleImageView;

@FragmentWithArgs
public class PersonDetailsFragment extends BaseLceFragment<TextView, Person, PersonDetailsView, PersonDetailsPresenter>
        implements PersonDetailsView {

    @Arg
    String idPerson;
    @Arg
    String namePerson;

    @BindView(R.id.name)
    TextView name;
    @BindView(R.id.ratings)
    TextView ratings;
    @BindView(R.id.travels)
    TextView travels;
    @BindView(R.id.phone)
    TextView phone;
    @BindView(R.id.verified_facebook)
    TextView verifiedFacebook;
    @BindView(R.id.verified_email)
    TextView verifiedEmail;
    @BindView(R.id.description)
    EditText description;
    @BindView(R.id.location)
    TextView location;
    @BindView(R.id.age)
    TextView age;
    @BindView(R.id.member_since)
    TextView memberSince;
    @BindView(R.id.ocupation)
    TextView ocupation;
    @BindView(R.id.profileHeaderPic)
    CircleImageView profileImage;

    private Person person;
    private PersonDetailsComponent personDetailsComponent;

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    @Override
    protected int getLayoutRes() {

        return R.layout.fragment_details_person;
    }

    @Override
    public Person getData() {
        return person;
    }

    @Override
    public PersonDetailsPresenter createPresenter() {
        return personDetailsComponent.presenter();
    }

    @NonNull
    @Override
    public LceViewState<Person, PersonDetailsView> createViewState() {
        return new ParcelableDataLceViewState<>();
    }


    @Override
    protected String getErrorMessage(Throwable e, boolean pullToRefresh) {
        return "Error Travel Details Fragment";
    }

    @Override
    public void setData(Person data) {

        this.person = data;


        name.setText(person.getName() + " " + person.getSurname());
        profileImage.setImageResource(R.drawable.com_facebook_profile_picture_blank_square);
        description.setText(person.getDescription());
        ratings.setText(String.valueOf(person.getRatings()));
        travels.setText(String.valueOf(person.getTravels()));
        phone.setText(person.getPhone());

        if(person.isVerifiedFacebook()){
            verifiedFacebook.setTextColor(getResources().getColor(R.color.green));
            verifiedFacebook.setText(getString(R.string.text_verified));
        }else {
            verifiedFacebook.setTextColor(getResources().getColor(R.color.red));
            verifiedFacebook.setText(getString(R.string.text_no_verified));
        }

        if(person.isVerifiedEmail()){
            verifiedEmail.setTextColor(getResources().getColor(R.color.green));
            verifiedEmail.setText(getString(R.string.text_verified));
        }else {
            verifiedEmail.setTextColor(getResources().getColor(R.color.red));
            verifiedEmail.setText(getString(R.string.text_no_verified));
        }
        location.setText(person.getLocation());
        ocupation.setText(person.getOcupation());
        memberSince.setText(Dates.transformDateStringToDateBase(person.getMemberSince()));
        age.setText(String.valueOf(Dates.calculateAge(Dates.convertDateStringToCalendar(person.getBirthdate()))));

    }

    @Override
    public void loadData(boolean pullToRefresh) {
        presenter.loadPerson(idPerson);


    }

    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);

        //getActivity().getActionBar().setTitle(locationOrigin + " - " + locationDestination);

    }


    @Override
    protected void injectDependencies() {
        personDetailsComponent =
                DaggerPersonDetailsComponent.builder()
                        .shareAppComponent(ShareApplication.getShareComponents())
                        .build();

        personDetailsComponent.inject(this);
    }

}
