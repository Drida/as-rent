package com.drida.asrent.presentation.travels;

import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentPagerAdapter;
import android.support.v4.app.FragmentStatePagerAdapter;


import com.drida.asrent.domain.model.contact.Person;
import com.drida.asrent.domain.model.travel.TravelScreen;
import com.drida.asrent.presentation.travels.travelspublished.TravelsPublishedFragment;
import com.drida.asrent.presentation.travels.travelsrequested.TravelsRequestedFragment;

import java.util.List;

public class TravelsScreenAdapter extends FragmentStatePagerAdapter {

    List<TravelScreen> screens;

    public TravelsScreenAdapter(FragmentManager fm) {

        super(fm);
    }

    public List<TravelScreen> getScreens() {
        return screens;
    }

    public void setScreens(List<TravelScreen> screens) {
        this.screens = screens;
    }

    @Override
    public Fragment getItem(int position) {
        TravelScreen screen = screens.get(position);

        if(screen.getType() == TravelScreen.TYPE_PUBLISHED){
            return new TravelsPublishedFragment();
        }

        if(screen.getType() == TravelScreen.TYPE_REQUESTED){
            return new TravelsRequestedFragment();
        }

       throw  new RuntimeException("Exception en TravelScreenAdapter");
    }

    @Override
    public int getCount() {
        return screens == null ? 0 : screens.size();
    }

    @Nullable
    @Override
    public CharSequence getPageTitle(int position) {
        return screens.get(position).getName();
    }
}
