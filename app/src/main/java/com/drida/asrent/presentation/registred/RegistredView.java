package com.drida.asrent.presentation.registred;

import com.hannesdorfmann.mosby3.mvp.MvpView;

public interface RegistredView extends MvpView {

    public void showRegistredForm();

    public void showError();

    public void showLoading();

    public void registredSuccesfull();
}
