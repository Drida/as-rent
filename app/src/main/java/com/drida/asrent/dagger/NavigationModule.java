package com.drida.asrent.dagger;

import com.drida.asrent.utils.IntentStarter;

import javax.inject.Singleton;

import dagger.Module;
import dagger.Provides;

@Module
public class NavigationModule {

    @Singleton @Provides
    public IntentStarter providesIntentStarter() {
        return new IntentStarter();
    }
}
