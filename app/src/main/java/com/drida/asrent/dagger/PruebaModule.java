package com.drida.asrent.dagger;

import org.greenrobot.eventbus.EventBus;

import javax.inject.Singleton;

import dagger.Module;
import dagger.Provides;

@Module
public class PruebaModule {

    @Singleton
    @Provides
    public EventBus providesEventBus(){
        return EventBus.getDefault();
    }

}
