package com.drida.asrent;


import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.design.widget.BottomNavigationView;
import android.support.v4.app.Fragment;
import android.util.Log;
import android.view.MenuItem;


import com.drida.asrent.domain.model.account.AccountManager;
import com.drida.asrent.presentation.base.view.BaseActivity;
import com.drida.asrent.presentation.messages.MessagesFragment;
import com.drida.asrent.presentation.options.OptionsFragment;
import com.drida.asrent.presentation.profile.ProfileFragment;
import com.drida.asrent.presentation.travels.TravelsFragment;
import com.drida.asrent.utils.Dates;
import com.drida.asrent.utils.IntentStarter;


import javax.inject.Inject;

import icepick.Icepick;

public class MainActivity extends BaseActivity {

    private MainActivityComponent mainActivityComponent;

    public static final String KEY_SHOW_ACTION = "com.drida.asrent.MainActivity.SHOW_ACTION";

    public static final String KEY_SHOW_ACTION_TRAVELS_OF_PERSON = "com.drida.asrent.MainActivity.TRAVELS_OF_PERSON";

    public static final String FRAGMENT_TAG_CONTAINER = "containerFragmentTag";

    ProfileFragment profileFragment;

    Fragment travelsFragment;

    @Inject
    IntentStarter intentStarter;

    @Inject
    AccountManager accountManager;


    private BottomNavigationView.OnNavigationItemSelectedListener mOnNavigationItemSelectedListener
            = new BottomNavigationView.OnNavigationItemSelectedListener() {

        @Override
        public boolean onNavigationItemSelected(@NonNull MenuItem item) {

            switch (item.getItemId()) {
                case R.id.navigation_travels:
                    showTravels();
                    return true;
                case R.id.navigation_options:
                    showOptions();
                    return true;
                case R.id.navigation_messages:
                    showMessages();
                    return true;
                case R.id.navigation_profile:
                    showProfile();
                    return true;
            }
            return false;
        }
    };


    private void checkLogin() {


        if (!accountManager.isUserAuthenticated())
            intentStarter.showAuthentication(this);


    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        getSupportActionBar().setDisplayHomeAsUpEnabled(false);

        checkLogin();

        showTravels();

        BottomNavigationView navigation = (BottomNavigationView) findViewById(R.id.navigation);
        navigation.setOnNavigationItemSelectedListener(mOnNavigationItemSelectedListener);




        String fechaPrueba = "20180620";
        String horaPrueba = "20:40";

        if(Dates.isDateAvailable(fechaPrueba, horaPrueba)){
            Log.d("ENTRO", "ENTRO FECHA VALIADA");
        }else {
            Log.d("ENTRO", "ENTRO FECHA NO VALIADA");
        }
    }



    public void showTravels() {

        getSupportActionBar().setTitle(getString(R.string.title_travels));
        getSupportFragmentManager().beginTransaction().replace(R.id.fragmentContainer, new TravelsFragment()).commit();


    }


    public void showOptions() {

        getSupportActionBar().setTitle(getString(R.string.title_options));

        getSupportFragmentManager().beginTransaction().replace(R.id.fragmentContainer, new OptionsFragment()).commit();


    }

    public void showMessages() {


        getSupportActionBar().setTitle(getString(R.string.title_messages));
        getSupportFragmentManager().beginTransaction().replace(R.id.fragmentContainer, new MessagesFragment()).commit();


    }

    public void showProfile() {


        getSupportActionBar().setTitle(getString(R.string.title_profile));
        getSupportFragmentManager().beginTransaction().replace(R.id.fragmentContainer, new ProfileFragment()).commit();


    }

    @Override
    protected void onSaveInstanceState(Bundle outState) {
        super.onSaveInstanceState(outState);
        Icepick.saveInstanceState(this, outState);
    }

    @Override
    protected void onNewIntent(Intent intent) {
        super.onNewIntent(intent);

       showTravels();

    }

    @Override
    protected void injectDependencies() {
        mainActivityComponent = DaggerMainActivityComponent.create();
        mainActivityComponent.inject(this);

    }


}
