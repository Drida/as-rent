package com.drida.asrent;

import android.app.Application;
import android.content.Context;
import android.util.Log;

import com.drida.asrent.dagger.DaggerShareAppComponent;
import com.drida.asrent.dagger.ShareAppComponent;
import com.squareup.leakcanary.LeakCanary;
import com.squareup.leakcanary.RefWatcher;

public class ShareApplication extends Application {

    private RefWatcher refWatcher;

    private static ShareAppComponent shareAppComponent;

    @Override
    public void onCreate() {

        super.onCreate();


        if (LeakCanary.isInAnalyzerProcess(this)) {


            return;
        }
        shareAppComponent = DaggerShareAppComponent.create();
        refWatcher = LeakCanary.install(this);
    }

    public static RefWatcher getRefWatcher(Context context) {
        ShareApplication application = (ShareApplication) context.getApplicationContext();
        return application.refWatcher;
    }

    public static ShareAppComponent getShareComponents() {
        return shareAppComponent;
    }
}
