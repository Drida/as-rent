package com.drida.asrent.domain.model.contact;


import com.drida.asrent.data.specification.firebase.PersonByIdSpecification;


import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.Callable;
import java.util.concurrent.TimeUnit;

import javax.inject.Inject;
import javax.inject.Named;

import io.reactivex.Observable;
import io.reactivex.ObservableSource;
import io.reactivex.Observer;
import io.reactivex.disposables.Disposable;


public interface ContactsManager {


    Observable<Person> getPerson(String idPerson);
    Observable<Person> updatePerson(Person person);


}
